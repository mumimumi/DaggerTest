package kap.daggerjava;

import dagger.Module;
import dagger.Provides;

@Module
abstract class MainActivityModule {
    @Provides
    @PerActivity
    static Talker provideTalker(){
        return new Talker();
    }

}
