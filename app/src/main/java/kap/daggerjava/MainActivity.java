package kap.daggerjava;

import android.os.Bundle;

import javax.inject.Inject;


public class MainActivity extends BaseActivity {
    @Inject
    Talker talker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        talker.speak();
    }
}
